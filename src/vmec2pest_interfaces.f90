!******************************************************************************
! This module contains routines that allow other codes to interface with 
! routines in VMECTools.
! Author: B.J. Faber, University of Wisconsin-Madsion (bfaber@wisc.edu)
! Creation date: June 2019
!******************************************************************************

module vmec2pest_interfaces
  use, intrinsic :: iso_c_binding
  use types, only: dp
  use pest_object, only: PEST_Obj, create_PEST_Obj, set_PEST_reference_values, get_PEST_data
  use compute_pest, only: compute_pest_geometry
  use normalizations, only: set_normalizations 
  implicit none

  public :: vmec2pest_interface, vmec2pest_c_interface, vmec2pest_stellopt_interface, get_pest_data_interface

  private
    type(PEST_Obj) :: pest ! Make this pest_object shared within the module so data can be read using get_PEST_data

    type, bind(C) :: vmec2pest_c_options
      ! Currently this must be a NetCDF file
      type(c_ptr) :: geom_file
      type(c_ptr) :: grid_type
      type(c_ptr) :: norm_type
      integer(c_int) :: n_psi, n_alpha, n_zeta
      type(c_ptr) :: psi
      real(c_double) :: alpha_center, zeta_center, nfpi
      !integer(c_int) :: npol
    end type

  interface c2f
    module procedure c2f_string_array_1d
    module procedure c2f_real_array_1d
  end interface 

contains
  !****************************************************************************
  ! Interface to call vmec2pest from C/C++
  !****************************************************************************
  subroutine vmec2pest_c_interface(options) bind(C,name='vmec2pest_c_interface')
    use, intrinsic :: iso_c_binding
    type(vmec2pest_c_options), intent(in) :: options
    character(len=:), allocatable, target :: geom_file
    character(len=:), pointer :: geom_id
    real(c_double), dimension(:), allocatable :: surfaces
    integer :: surf_opt, i
    character(len=:), allocatable :: grid_type, norm_type
    real(dp) :: nfpif
    surf_opt = 0

    call c2f(options%geom_file,geom_file)
    geom_id => geom_file(1:len(geom_file))
    call c2f(options%psi,surfaces,options%n_psi)
    pest = create_PEST_Obj(geom_id,surfaces,options%n_psi,options%n_alpha,options%n_zeta)
 
    call c2f(options%grid_type,grid_type)
    call c2f(options%norm_type,norm_type)

    call set_PEST_reference_values(pest,norm_type)
    pest%zeta_max_interval = options%nfpi
    call compute_pest_geometry(pest,options%alpha_center,options%zeta_center,nfpif,surf_opt)
    call set_normalizations(pest,trim(grid_type))

    deallocate(geom_file,grid_type,norm_type,surfaces)
    
  end subroutine

  subroutine get_pest_data_c_interface(x1,x2,data_name,n_dims,data_size,c_data) bind(C,name="get_pest_data_c_interface")
    use, intrinsic :: iso_c_binding
    integer(c_int), intent(in) :: n_dims, data_size, x1, x2
    type(c_ptr), intent(in) :: data_name
    real(c_double), dimension(data_size), intent(out) :: c_data
    real(dp) :: vmec_data, rad_data
    real(dp), dimension(:), allocatable :: line_data
    real(dp), dimension(:,:), allocatable :: surf_data
    real(dp), dimension(:,:,:), allocatable :: vol_data 
    character(len=:), allocatable :: data_string
    integer idx, idx1, idx2, idx3, str_len;

    call c2f(data_name,data_string)

    ! TODO: implement bounds checking on x1 and x2 to ensure they aren't outside the dimension of
    ! the PEST object
    select case(n_dims)
      case(-1)
        if (data_size .ne. 1) then
          print *, "VMEC2PEST Error! C array does not have the same size as 1!"
          stop
        endif
        call get_PEST_data(pest,trim(data_string),vmec_data)
        c_data(1) = vmec_data
      case(0)
        if (data_size .ne. 1) then
          print *, "VMEC2PEST Error! C array does not have the same size as 1!"
          stop
        endif
        call get_PEST_data(pest,x1,trim(data_string),rad_data)
        c_data(1) = rad_data
      case(1)
        if (data_size .ne. pest%n_zeta) then
          print *, "VMEC2PEST Error! C array does not have the same size as ",pest%n_zeta,"!"
          stop
        end if
        allocate(line_data(pest%idx_zeta_1:pest%idx_zeta_2))
        call get_PEST_data(pest,x1,x2,trim(data_string),line_data)
        do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
          idx = idx3 - pest%idx_zeta_1 + 1
          c_data(idx) = line_data(idx3)
        end do
        deallocate(line_data)
      case(2)
        if (data_size .ne. pest%n_alpha*pest%n_zeta) then
          print *, "VMEC2PEST Error! C array does not have the same size as ",pest%n_alpha*pest%n_zeta,"!"
          stop
        end if
        allocate(surf_data(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2))
        call get_PEST_data(pest,x1,trim(data_string),surf_data)
        do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
          do idx2 = pest%idx_alpha_1,pest%idx_alpha_2
            idx = pest%n_alpha*(idx3 - pest%idx_zeta_1) + idx2 - pest%idx_alpha_1 + 1 
            c_data(idx) = surf_data(idx3,idx2)
          end do
        end do
        deallocate(surf_data)
      case(3)
        if (data_size .ne. pest%n_psi*pest%n_alpha*pest%n_zeta) then
          print *, "VMEC2PEST Error! C array does not have the same size as ",pest%n_psi*pest%n_alpha*pest%n_zeta,"!"
          stop
        end if
        allocate(vol_data(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2,pest%idx_psi_1:pest%idx_psi_2))
        call get_PEST_data(pest,trim(data_string),vol_data)
        do idx1 = pest%idx_psi_1,pest%idx_psi_2
          do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
            do idx2 = pest%idx_alpha_1,pest%idx_alpha_2
              idx = pest%n_alpha*pest%n_zeta*(idx1 - pest%idx_psi_1) + pest%n_alpha*(idx3 - pest%idx_zeta_1) + idx2 - pest%idx_alpha_1 + 1 
              c_data(idx) = vol_data(idx3,idx2,idx1)
            end do
          end do
        end do
        deallocate(vol_data)
      case default
        print *, "VMEC2PEST Error! n_dims must be specified to 1, 2, or 3!"
        stop
    end select

    deallocate(data_string)

  end subroutine

  subroutine c2f_string_array_1d(c_pointer,f_string)
    use, intrinsic :: iso_c_binding
    type(c_ptr), intent(in) :: c_pointer
    character(len=:), allocatable, intent(out) :: f_string
    character, dimension(:), pointer :: f_pointer
    character(len=:), pointer :: f_char_pointer

    integer :: i
    logical :: null_char_found
    i = 0
    null_char_found = .false.
    do while (null_char_found .eqv. .false.)
      i = i + 1
      call c_f_pointer(c_pointer,f_pointer,[i])
      if (f_pointer(i) == c_null_char) then
        null_char_found = .true.
      end if
    end do
    i = i - 1
    
    allocate(character(len=i)::f_string)
    call c_f_pointer(c_loc(f_pointer),f_char_pointer)

    f_string = f_char_pointer(1:i)

  end subroutine

  subroutine c2f_real_array_1d(c_pointer,f_real,data_size)
    use, intrinsic :: iso_c_binding
    type(c_ptr), intent(in) :: c_pointer
    real(dp), dimension(:), allocatable, intent(out) :: f_real
    integer, intent(in) :: data_size
    real(dp), dimension(:), pointer :: f_pointer

    call c_f_pointer(c_pointer,f_pointer,[data_size])

    allocate(f_real(data_size))
    f_real = f_pointer(1:data_size)
  end subroutine
 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Interface for calling vmec2pest from STELLOPT
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine vmec2pest_stellopt_interface(surfaces,n_alpha,n_zeta,alpha_center,zeta_center,nfpi,npol,norm_type,grid_type)
    character(*), intent(in) :: norm_type, grid_type
    real(dp), dimension(:), intent(in) :: surfaces
    real(dp), intent(in) :: alpha_center, zeta_center, nfpi
    integer, intent(in) :: n_alpha, n_zeta, npol
    character(len=:), pointer :: geom_id
    real(dp) :: nfpif
    
    geom_id => NULL()
    pest = create_PEST_Obj(geom_id,surfaces,size(surfaces),n_alpha,n_zeta)
    call set_PEST_reference_values(pest,norm_type)
    pest%zeta_max_interval = nfpi
    call compute_pest_geometry(pest,alpha_center,zeta_center,nfpif,0)
    call set_normalizations(pest,grid_type) 
    
  end subroutine

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Interface for calling vmec2pest from Fortran
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine vmec2pest_interface(geom_file,surfaces,n_alpha,n_zeta,alpha_center,zeta_center,nfpi,npol,norm_type,grid_type)
    character(*), intent(in), target :: geom_file
    character(*), intent(in) :: norm_type, grid_type
    real(dp), dimension(:), intent(in) :: surfaces
    real(dp), intent(in) :: alpha_center, zeta_center, nfpi
    integer, intent(in) :: n_alpha, n_zeta, npol
    character(len=:), pointer :: geom_id
    real(dp) :: nfpif
    
    geom_id => geom_file
    pest = create_PEST_Obj(geom_id,surfaces,size(surfaces),n_alpha,n_zeta)
    call set_PEST_reference_values(pest,norm_type)
    call compute_pest_geometry(pest,alpha_center,zeta_center,nfpif,0)
    call set_normalizations(pest,grid_type) 
    

  end subroutine

  subroutine get_pest_data_interface(x1,x2,data_string,n_dims,data_size,data_arr)
    integer, intent(in) :: n_dims, data_size, x1, x2
    character(*), intent(in) :: data_string
    real(dp), dimension(data_size), intent(out) :: data_arr
    real(dp) :: rad_data, vmec_data
    real(dp), dimension(:), allocatable :: line_data
    real(dp), dimension(:,:), allocatable :: surf_data
    real(dp), dimension(:,:,:), allocatable :: vol_data 
    integer idx, idx1, idx2, idx3


    ! TODO: implement bounds checking on x1 and x2 to ensure they aren't outside the dimension of
    ! the PEST object
    select case(n_dims)
      case(-1)
        if (data_size .ne. 1) then
          print *, "VMEC2PEST Error! Array does not have the same size as 1!"
          stop
        endif
        call get_PEST_data(pest,trim(data_string),vmec_data)
        data_arr(1) = vmec_data
      case(0)
        if (data_size .ne. 1) then
          print *, "VMEC2PEST Error! Array does not have the same size as 1!"
          stop
        endif
        call get_PEST_data(pest,x1,trim(data_string),rad_data)
        data_arr(1) = rad_data
      case(1)
        if (data_size .ne. pest%n_zeta) then
          print *, "VMEC2PEST Error! Array does not have the same size as ",pest%n_zeta,"!"
          stop
        end if
        allocate(line_data(pest%idx_zeta_1:pest%idx_zeta_2))
        call get_PEST_data(pest,x1,x2,trim(data_string),line_data)
        do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
          idx = idx3 - pest%idx_zeta_1 + 1
          data_arr(idx) = line_data(idx3)
        end do
        deallocate(line_data)
      case(2)
        if (data_size .ne. pest%n_alpha*pest%n_zeta) then
          print *, "VMEC2PEST Error! Array does not have the same size as ",pest%n_alpha*pest%n_zeta,"!"
          stop
        end if
        allocate(surf_data(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2))
        call get_PEST_data(pest,x1,trim(data_string),surf_data)
        do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
          do idx2 = pest%idx_alpha_1,pest%idx_alpha_2
            idx = pest%n_alpha*(idx3 - pest%idx_zeta_1) + idx2 - pest%idx_alpha_1 + 1 
            data_arr(idx) = surf_data(idx3,idx2)
          end do
        end do
        deallocate(surf_data)
      case(3)
        if (data_size .ne. pest%n_psi*pest%n_alpha*pest%n_zeta) then
          print *, "VMEC2PEST Error! Array does not have the same size as ",pest%n_psi*pest%n_alpha*pest%n_zeta,"!"
          stop
        end if
        allocate(vol_data(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2,pest%idx_psi_1:pest%idx_psi_2))
        call get_PEST_data(pest,trim(data_string),vol_data)
        do idx1 = pest%idx_psi_1,pest%idx_psi_2
          do idx3 = pest%idx_zeta_1,pest%idx_zeta_2
            do idx2 = pest%idx_alpha_1,pest%idx_alpha_2
              idx = pest%n_alpha*pest%n_zeta*(idx1 - pest%idx_psi_1) + pest%n_alpha*(idx3 - pest%idx_zeta_1) + idx2 - pest%idx_alpha_1 + 1 
              data_arr(idx) = vol_data(idx3,idx2,idx1)
            end do
          end do
        end do
        deallocate(vol_data)
      case default
        print *, "VMEC2PEST Error! n_dims must one of the following:"
        print *, "n_dims = -1 - VMEC data"
        print *, "n_dims = 0 - radial data"
        print *, "n_dims = 1 - field line data"
        print *, "n_dims = 2 - flux surface data"
        print *, "n_dims = 3 - volume data"
        stop
    end select

  end subroutine

end module
