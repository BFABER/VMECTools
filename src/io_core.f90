!*******************************************************************************
! This module defines the I/O routines used in the vmec2pest calculation
! Author: B.J. Faber (bfaber@wisc.edu)
! Creation date: 30.08.2018
!******************************************************************************

module io_core
  use types, only: dp, pi
  use pest_object, only: PEST_Obj
  use hdf5
  implicit none

  public:: read_vmec2pest_input, write_pest_file, write_cylindrical_surface, write_RZ_theta_zeta_grid, &
    & write_gene_geometry_file, write_surface_quantity_cyl, write_surface_quantity_xyz, write_surface_quantity_theta_zeta, &
    & create_mapping_file_hdf5, close_mapping_file_hdf5, write_mapping_file_info_hdf5, write_mapping_file_surf_data_hdf5, & 
    & write_nonuniform_mapping_file_info_hdf5, write_nonuniform_mapping_file_surf_data_hdf5
  public:: tag, geom_file, outdir, norm_type, &
    & n_surf, n_field_lines, n_parallel_pts, n_pol, alpha_center, zeta_center, &
    & n_field_periods, surfaces, surf_opt, verbose, test, &
    & output_files, surface_quantities, n_surface_quantities, geom_id

  private 
    character(len=:), pointer :: geom_id
    character(len=2000), target :: geom_file
    character(len=2000) :: tag, outdir
    character(len=7) :: norm_type
    integer :: n_surf, n_field_lines, n_parallel_pts, surf_opt, n_surface_quantities, n_pol
    real(dp) :: alpha_center, zeta_center, n_field_periods
    real(dp), dimension(501) :: surfaces
    character(len=4), dimension(4) :: output_files
    character(len=32), dimension(24) :: surface_quantities
    logical :: verbose, test

contains
  
  subroutine read_vmec2pest_input(filename)
    ! Reads the parameters from the input file
    implicit none
    character(len=256), intent(in) :: filename
    character(len=:), allocatable, target :: temp
    character(len=32) :: temp_str
    integer :: iunit, j
    logical :: end_found

    namelist /parameters/ tag, geom_file, outdir, norm_type, &
      & n_field_lines, n_parallel_pts, alpha_center, zeta_center, n_field_periods, n_pol, &
      & surfaces, surf_opt, verbose, test, output_files, surface_quantities
    ! Set default values of parameters
    tag = ''
    norm_type = 'minor_r'
    outdir = ''
    
    n_field_lines = 1
    n_parallel_pts = 128
    alpha_center = 0.0
    zeta_center = 0.0  

    n_field_periods = -1.0 ! Number of field periods to calculate
    n_pol = -1
   
    surfaces(1) = 0.5
    surf_opt = 0 
    verbose = .false.
    test = .false.
    output_files(1) = 'pest'
    surface_quantities = "stop" 

    ! Add line to ensure iunit is already not open
    open(newunit=iunit,file=trim(filename),status='old',action='read')
    read(iunit,NML=parameters)
    close(iunit)

    do j = 1,size(surfaces)
      if (isnan(surfaces(j))) then
        exit
      else
        if (surfaces(j) .lt. 1e-8) then 
          exit
        else if (surfaces(j) .gt. 1.0) then
          exit
        end if
      end if
    end do
    n_surf = j-1

    j = 0

    block
      character(:), allocatable :: temp_str
      end_found = .false.
      do while(end_found .eqv. .false.)
        j = j + 1
        temp_str = trim(surface_quantities(j))
        if (temp_str == 'stop') then
          end_found = .true.
        end if
      end do
    end block

    n_surface_quantities = j-1

    geom_id => geom_file(1:len(trim(geom_file)))

  end subroutine

  subroutine write_pest_file(pest,idx1,n_field_periods_final)
    implicit none
    integer, intent(in) :: idx1
    type(PEST_Obj), intent(in) :: pest
    real(dp), intent(in) :: n_field_periods_final
    integer :: j, k, iunit
    real(dp) :: prefac
    character(len=2000) :: filename
    character(len=2000) :: filenumber
    character(len=6) :: x3_string

    prefac = 1.0

    write(filenumber,"(I0.3)") idx1
    filename = trim(outdir)//"pest_"//trim(tag)//"_surf_"//trim(filenumber)//".dat"
    open(newunit=iunit,file=trim(filename))
    write (iunit,'(A)') '&parameters'
    write (iunit,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1)
    write (iunit,'(A,F12.7)') '!minor_r = ', pest%L_ref
    write (iunit,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit,'(A,I6)') 'gridpoints = ', pest%n_zeta-1
    write (iunit,'(A,F8.3)') 'n_pol = ', pest%iota(idx1)*n_field_periods_final/pest%vmec%nfp
    write (iunit,'(A,F8.3)') 'n_tor = ', n_field_periods_final/pest%vmec%nfp
    write (iunit,'(A)') '/'
    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        write(iunit,'(36(ES23.12E3,2x))') prefac*pest%zeta(k,j,idx1)/pi, pest%grad_psi_X(k,j,idx1), pest%grad_psi_Y(k,j,idx1), &
          & pest%grad_psi_Z(k,j,idx1), pest%grad_alpha_X(k,j,idx1), pest%grad_alpha_Y(k,j,idx1), pest%grad_alpha_Z(k,j,idx1), &
          & pest%grad_zeta_X(k,j,idx1), pest%grad_zeta_Y(k,j,idx1), pest%grad_zeta_Z(k,j,idx1), &
          & pest%basis_psi_X(k,j,idx1), pest%basis_psi_Y(k,j,idx1), pest%basis_psi_Z(k,j,idx1), &
          & pest%basis_alpha_X(k,j,idx1), pest%basis_alpha_Y(k,j,idx1), pest%basis_alpha_Z(k,j,idx1), &
          & pest%basis_zeta_X(k,j,idx1), pest%basis_zeta_Y(k,j,idx1), pest%basis_zeta_Z(k,j,idx1), &
          & pest%B_X(k,j,idx1), pest%B_Y(k,j,idx1), pest%B_Z(k,j,idx1), pest%grad_B_X(k,j,idx1), &
          & pest%grad_B_Y(k,j,idx1), pest%grad_B_Z(k,j,idx1), pest%g_psi_psi(k,j,idx1), pest%g_psi_alpha(k,j,idx1), pest%g_alpha_alpha(k,j,idx1), &
          & pest%g_psi_zeta(k,j,idx1), pest%g_alpha_zeta(k,j,idx1), pest%g_zeta_zeta(k,j,idx1), pest%bmag(k,j,idx1), pest%jacobian(k,j,idx1), &
          & pest%normal_curv(k,j,idx1), pest%geodesic_curv(k,j,idx1), pest%d_B_d_zeta(k,j,idx1) 
      end do
    end do
    close(iunit)

  end subroutine

  subroutine write_gene_geometry_file(pest,idx1,n_field_periods_final)
    implicit none
    integer, intent(in) :: idx1
    type(PEST_Obj), intent(in) :: pest
    real(dp), intent(in) :: n_field_periods_final
    integer :: j, k, iunit
    real(dp) :: prefac
    character(len=2000) :: filename
    character(len=2000) :: filenumber

    prefac = 1.0

    write(filenumber,"(I0.3)") idx1
    filename = trim(outdir)//"gene_"//trim(tag)//"_surf_"//trim(filenumber)//".dat"
    open(newunit=iunit,file=trim(filename))
    write (iunit,'(A)') '&parameters'
    write (iunit,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1)
    write (iunit,'(A,F12.7)') '!alpha0 = ', pest%alpha(1,pest%idx_alpha_1,idx1)
    write (iunit,'(A,F12.7)') '!minor_r = ', pest%L_ref
    write (iunit,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit,'(A,I6)') 'gridpoints = ', pest%n_zeta-1
    write (iunit,'(A,I6)') 'n_pol = ', ceiling(pest%iota(idx1)*n_field_periods_final/pest%vmec%nfp)
    write (iunit,'(A)') '/'

    do k=pest%idx_zeta_1,pest%idx_zeta_2-1
      do j=pest%idx_alpha_1,pest%idx_alpha_2
        write(iunit,'(9(ES23.12E3,2x))') pest%g_psi_psi(k,j,idx1), pest%g_psi_alpha(k,j,idx1), pest%g_alpha_alpha(k,j,idx1), &
          & pest%bmag(k,j,idx1), pest%jacobian(k,j,idx1), pest%grad_B_drift_psi(k,j,idx1), pest%grad_B_drift_alpha(k,j,idx1), &
          & pest%d_B_d_zeta(k,j,idx1), prefac*pest%zeta(k,j,idx1)/pi
        end do
    end do
    close(iunit)

  end subroutine

  subroutine write_cylindrical_surface(pest,idx1)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    integer :: j, k, iunit_cyl
    character(len=2000) :: filename_cyl, filenumber
    write(filenumber,"(I0.3)") idx1

    filename_cyl = trim(outdir)//"cyl_surface_"//trim(tag)//"_surf_"//trim(filenumber)//".dat"
    open(file=trim(filename_cyl),newunit=iunit_cyl)
    write (iunit_cyl,'(A)') '&parameters'
    write (iunit_cyl,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_cyl,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_cyl,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_cyl,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_cyl,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_cyl,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_cyl,'(A)') '/'
    write (iunit_cyl,'(3(A12))') '# R', 'Z', 'Phi'

    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        write (iunit_cyl,'(3(F12.7,2x))') pest%Rsurf(k,j,idx1), pest%Zsurf(k,j,idx1), -pest%zeta(k,j,idx1)
      end do
      write (iunit_cyl,'(A)') " "
    end do
    close(iunit_cyl)
  end subroutine

  subroutine write_surface_quantity_cyl(pest,idx1,data_name,surf_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(len=32), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2), intent(in) :: surf_data
    integer :: j, k, iunit_cyl
    character(len=2000) :: filename_cyl, filenumber
    write(filenumber,"(I0.3)") idx1


    filename_cyl = trim(outdir)//"cyl_surface_"//trim(tag)//"_"//trim(data_name)//"_surf_"//trim(filenumber)//".dat"
    open(file=trim(filename_cyl),newunit=iunit_cyl)
    write (iunit_cyl,'(A)') '&parameters'
    write (iunit_cyl,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_cyl,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_cyl,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_cyl,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_cyl,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_cyl,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_cyl,'(A)') '/'
    write (iunit_cyl,'(3(A12))') '# R', 'Z', 'Phi'

    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        write (iunit_cyl,'(4(F12.7,2x))') pest%Rsurf(k,j,idx1), pest%Zsurf(k,j,idx1), pest%zeta(k,j,idx1), surf_data(k,j)
      end do
      write (iunit_cyl,'(A)') " "
      
      write (iunit_cyl,'(4(F12.7,2x))') pest%Rsurf(k,pest%idx_alpha_1,idx1), pest%Zsurf(k,pest%idx_alpha_1,idx1), pest%zeta(k,j,idx1), surf_data(pest%idx_alpha_1,k)
    end do
    close(iunit_cyl)
  end subroutine

  subroutine write_surface_quantity_xyz(pest,idx1,data_name,surf_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(len=32), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2), intent(in) :: surf_data
    integer :: j, k, iunit_xyz
    character(len=2000) :: filename_xyz, filenumber
    write(filenumber,"(I0.3)") idx1

    filename_xyz = trim(outdir)//"xyz_surface_"//trim(tag)//"_"//trim(data_name)//"_surf_"//trim(filenumber)//".dat"
    open(file=trim(filename_xyz),newunit=iunit_xyz)
    write (iunit_xyz,'(A)') '&parameters'
    write (iunit_xyz,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_xyz,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_xyz,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_xyz,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_xyz,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_xyz,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_xyz,'(A)') '/'
    write (iunit_xyz,'(3(A12))') '# ', 'X', 'Y', 'Z' 

    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2
        write (iunit_xyz,'(4(F12.7,2x))') pest%Rsurf(k,j,idx1)*cos(pest%zeta(k,j,idx1)), pest%Rsurf(k,j,idx1)*sin(pest%zeta(k,j,idx1)), pest%Zsurf(k,j,idx1), surf_data(k,j)
      end do
    end do
    j=pest%idx_alpha_1
    do k=pest%idx_zeta_1,pest%idx_zeta_2
      write (iunit_xyz,'(4(F12.7,2x))') pest%Rsurf(k,j,idx1)*cos(pest%zeta(k,j,idx1)), pest%Rsurf(k,j,idx1)*sin(pest%zeta(k,j,idx1)), pest%Zsurf(k,j,idx1), surf_data(k,j)
    end do
    write (iunit_xyz,'(A)') " "
    k=pest%idx_zeta_1
    do j=pest%idx_alpha_1,pest%idx_alpha_2
      write (iunit_xyz,'(4(F12.7,2x))') pest%Rsurf(k,j,idx1)*cos(pest%zeta(k,j,idx1)), &
      & pest%Rsurf(k,j,idx1)*sin(pest%zeta(k,j,idx1)), pest%Zsurf(k,j,idx1), surf_data(k,j)
    end do
    j=pest%idx_alpha_1
    write (iunit_xyz,'(4(F12.7,2x))') pest%Rsurf(k,j,idx1)*cos(pest%zeta(k,j,idx1)), pest%Rsurf(k,j,idx1)*sin(pest%zeta(k,j,idx1)), &
    & pest%Zsurf(k,j,idx1), surf_data(k,j)
    write (iunit_xyz,'(A)') " "

    close(iunit_xyz)
  end subroutine

  subroutine write_surface_quantity_theta_zeta(pest,idx1,data_name,surf_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(len=32), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2), intent(in) :: surf_data
    integer :: i, j, jp1, k, iunit_tz, theta_index
    real(dp) :: surf_interp, theta, theta_j, theta_jp1, theta_interp, dt1, dt2, delta
    character(len=2000) :: filename_tz, filenumber
    real(dp), parameter :: pi2 = pi+pi
    real(dp), parameter :: eps = 1e-8

    write(filenumber,"(I0.3)") idx1


    filename_tz = trim(outdir)//"theta_zeta_surface_"//trim(tag)//"_"//trim(data_name)//"_surf_"//trim(filenumber)//".dat"
    open(file=trim(filename_tz),newunit=iunit_tz)
    write (iunit_tz,'(A)') '&parameters'
    write (iunit_tz,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_tz,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_tz,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_tz,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_tz,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_tz,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_tz,'(A)') '/'
    write (iunit_tz,'(5(A12))') '#theta_index', 'theta', 'zeta_index', 'zeta', trim(data_name)

    do k=pest%idx_zeta_1,pest%idx_zeta_2-1
      if (pest%n_alpha .gt. 1) then
        do j=pest%idx_alpha_1,pest%idx_alpha_2
          if (j .lt. pest%idx_alpha_2) then 
            jp1 = j+1 
          else 
            jp1 = pest%idx_alpha_1
          end if
          theta = 0.
          theta_j = pest%alpha(k,j,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
          theta_jp1 = pest%alpha(k,jp1,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
          if (theta_j .lt. (0.0 - eps)) then
            theta_j = pi2 + theta_j
          end if
          if (theta_jp1 .lt. (0.0 - eps)) then 
            theta_jp1 = pi2 + theta_jp1
          end if
          i = 0
          do while (theta .lt. (theta_j - eps))
            i = i + 1
            theta = real(i)*pi2/real(pest%n_alpha)
          end do
          theta_j = mod(theta_j,pi2)
          theta_jp1 = mod(theta_jp1,pi2)

          theta_index = mod(i,pest%n_alpha)
          theta_interp = real(theta_index)*pi2/real(pest%n_alpha)
          dt1 = theta_interp - theta_j
          if (dt1 .lt. -eps) then
            dt1 = dt1 + pi2
          end if
          dt2 = theta_jp1 - theta_interp
          if (dt2 .lt. -eps) then
            dt2 = dt2 + pi2
          end if       
          surf_interp = real(pest%n_alpha)/pi2*(dt2*surf_data(k,j) + dt1*surf_data(k,jp1))
          write (iunit_tz,'(2(I5,2x,F12.7,2x),F12.7)') theta_index+1, theta_interp, k+pest%n_zeta/2+1, pest%zeta(k,j,idx1), surf_interp
        end do
      else
        j = 1
        theta_index = 0
        theta_interp = pest%alpha(k,j,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)

        write (iunit_tz,'(2(I5,2x,F12.7,2x),F12.7)') theta_index+1, theta_interp, k+pest%n_zeta/2+1, &
        & pest%zeta(k,j,idx1),surf_data(k,j)
      end if
      write (iunit_tz,'(A)') " "
    end do

    close(iunit_tz)
  end subroutine

  subroutine write_RZ_theta_zeta_grid(pest,idx1,nfpi)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1, nfpi
    real(dp) :: theta, theta_j, theta_jp1, theta_interp, dt1, dt2, delta, Rsurf_interp, Zsurf_interp
    integer i, j, jp1, k, iunit_R, iunit_Z, theta_index
    character(len=2000) :: filename_R, filename_Z, filenumber
    real(dp), parameter :: pi2 = pi*pi
    real(dp), parameter :: eps = 1e-8

    write(filenumber,"(I0.3)") idx1
    delta = (real(pest%n_alpha)*nfpi)/(pest%vmec%nfp*real(pest%n_zeta/2))

    filename_R = trim(outdir)//"R_surface_"//trim(tag)//"_surf_"//trim(filenumber)//".dat"
    filename_Z = trim(outdir)//"Z_surface_"//trim(tag)//"_surf_"//trim(filenumber)//".dat"
    open(file=trim(filename_R),newunit=iunit_R)
    open(file=trim(filename_Z),newunit=iunit_Z)
    write (iunit_R,'(A)') '&parameters'
    write (iunit_R,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_R,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_R,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_R,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_R,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_R,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_R,'(A)') '/'
    write (iunit_Z,'(A)') '&parameters'
    write (iunit_Z,'(A,F12.7)') '!s0 = ', pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1) 
    write (iunit_Z,'(A,F12.7)') '!minor_r = ', pest%minor_r
    write (iunit_Z,'(A,F12.7)') '!major_R = ', pest%major_R
    write (iunit_Z,'(A,F12.7)') '!Bref = ', pest%B_ref
    write (iunit_Z,'(A,F12.7)') 'q0 = ', pest%safety_factor_q(idx1)
    write (iunit_Z,'(A,F12.7)') 'shat = ', pest%s_hat(idx1)
    write (iunit_Z,'(A)') '/'


    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        if (j .lt. pest%idx_alpha_2) then 
          jp1 = j+1 
        else 
          jp1 = pest%idx_alpha_1
        end if
        theta = 0.
        theta_j = pest%alpha(k,j,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        theta_jp1 = pest%alpha(k,jp1,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        if (theta_j .lt. (0.0 - eps)) then
          theta_j = pi2 + theta_j
        end if
        if (theta_jp1 .lt. (0.0 - eps)) then 
          theta_jp1 = pi2 + theta_jp1
        end if
        i = 0
        do while (theta .lt. (theta_j - eps))
          i = i + 1
          theta = real(i)*pi2/real(pest%n_alpha)
        end do
        theta_j = mod(theta_j,pi2)
        theta_jp1 = mod(theta_jp1,pi2)

        theta_index = mod(i,pest%n_alpha)
        theta_interp = real(theta_index)*pi2/real(pest%n_alpha)
        dt1 = theta_interp - theta_j
        if (dt1 .lt. -eps) then
          dt1 = dt1 + pi2
        end if
        dt2 = theta_jp1 - theta_interp
        if (dt2 .lt. -eps) then
          dt2 = dt2 + pi2
        end if       
        Rsurf_interp = real(pest%n_alpha)/pi2*(dt2*pest%Rsurf(k,j,idx1) + dt1*pest%Rsurf(k,jp1,idx1))
        Zsurf_interp = real(pest%n_alpha)/pi2*(dt2*pest%Zsurf(k,j,idx1) + dt1*pest%Zsurf(k,jp1,idx1))
        write (iunit_R,'(2(I5,2x),F12.7)') theta_index+1, k+pest%n_zeta/2+1, Rsurf_interp
        write (iunit_Z,'(2(I5,2x),F12.7)') theta_index+1, k+pest%n_zeta/2+1, Zsurf_interp
      end do
      write (iunit_R,'(A)') " "
      write (iunit_Z,'(A)') " "
    end do
    close(iunit_R)
    close(iunit_Z)
  end subroutine

  subroutine create_mapping_file_hdf5(filename,pest,file_id)
    character(len=*), intent(in) :: filename
    type(PEST_Obj), intent(in) :: pest
    integer(hid_t), intent(out) :: file_id

    integer(hid_t) :: space_id, data_id
    integer :: ierr
    integer, parameter :: one = 1
    integer(hsize_t), dimension(1) :: one_size = (/ 1 /)

    call h5open_f(ierr)

    call h5fcreate_f(trim(filename), H5F_ACC_TRUNC_F, file_id, ierr)

    call h5screate_simple_f(one, one_size, space_id, ierr)
    call h5dcreate_f(file_id, "B_ref", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%B_ref, one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Major R", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%major_R, one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Minor r", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%minor_r, one_size, ierr)
    call h5dclose_f(data_id, ierr)
    
    call h5dcreate_f(file_id, "Field periods", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, pest%vmec%nfp, one_size, ierr)
    call h5dclose_f(data_id, ierr)
    call h5sclose_f(space_id, ierr)

  end subroutine

  subroutine close_mapping_file_hdf5(file_id)
    integer(hid_t), intent(in) :: file_id
    integer :: ierr

    call h5fclose_f(file_id,ierr)
  end subroutine

  subroutine write_mapping_file_info_hdf5(file_id,pest,idx1)
    integer(hid_t), intent(in) :: file_id
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: theta_coords
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: zeta_coords
    integer, dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: theta_indices
    integer, dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: zeta_indices
    integer :: i, j, jp1, k, iunit_tz, theta_index, ierr
    real(dp) :: theta, theta_j, theta_jp1, theta_interp
    real(dp), parameter :: pi2 = pi+pi
    real(dp), parameter :: eps = 1e-8

    integer(hid_t) :: data_id, space_id
    integer, parameter :: rank = 2
    integer(hsize_t), dimension(2) :: dims
    integer, parameter :: one = 1
    integer(hsize_t), dimension(1) :: one_size = (/ 1 /)

    call h5screate_simple_f(one, one_size, space_id, ierr)
    call h5dcreate_f(file_id, "s0", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "iota", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%iota(idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "s_hat", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%s_hat(idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "M_theta", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, pest%n_alpha, one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "N_zeta", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, pest%n_zeta-1, one_size, ierr)
    call h5dclose_f(data_id, ierr)
    call h5sclose_f(space_id, ierr)

    dims(1) = pest%n_alpha
    dims(2) = pest%n_zeta-1

    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        if (j .lt. pest%idx_alpha_2) then 
          jp1 = j+1 
        else 
          jp1 = pest%idx_alpha_1
        end if
        theta = 0.
        theta_j = pest%alpha(k,j,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        theta_jp1 = pest%alpha(k,jp1,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        if (theta_j .lt. (0.0 - eps)) then
          theta_j = pi2 + theta_j
        end if
        if (theta_jp1 .lt. (0.0 - eps)) then 
          theta_jp1 = pi2 + theta_jp1
        end if
        i = 0
        do while (theta .lt. (theta_j - eps))
          i = i + 1
          theta = real(i)*pi2/real(pest%n_alpha)
        end do
        theta_j = mod(theta_j,pi2)
        theta_jp1 = mod(theta_jp1,pi2)

        theta_index = mod(i,pest%n_alpha)
        theta_interp = real(theta_index)*pi2/real(pest%n_alpha)
        theta_coords(k,j) = theta_interp
        zeta_coords(k,j) = pest%zeta(k,j,idx1)
        theta_indices(k,j) = theta_index+1
        zeta_indices(k,j) = k+pest%n_zeta/2+1
      end do
    end do

    call h5screate_simple_f(rank,dims,space_id,ierr) 
   
    call h5dcreate_f(file_id, "Theta", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, theta_coords, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Zeta", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, zeta_coords, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Theta Indices", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, theta_indices, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Zeta Indices", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, zeta_indices, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5sclose_f(space_id,ierr)

  end subroutine

  subroutine write_nonuniform_mapping_file_info_hdf5(file_id,pest,idx1)
    integer(hid_t), intent(in) :: file_id
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: theta_coords
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: zeta_coords
    integer, dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: theta_indices
    integer, dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: zeta_indices
    integer :: j, k, ierr

    integer(hid_t) :: data_id, space_id
    integer, parameter :: rank = 2
    integer(hsize_t), dimension(2) :: dims
    integer, parameter :: one = 1
    integer(hsize_t), dimension(1) :: one_size = (/ 1 /)

    call h5screate_simple_f(one, one_size, space_id, ierr)
    call h5dcreate_f(file_id, "s0", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "iota", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%iota(idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "s_hat", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, pest%s_hat(idx1), one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "M_theta", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, pest%n_alpha, one_size, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "N_zeta", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, pest%n_zeta-1, one_size, ierr)
    call h5dclose_f(data_id, ierr)
    call h5sclose_f(space_id, ierr)

    dims(1) = pest%n_alpha
    dims(2) = pest%n_zeta-1

    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        theta_coords(k,j) = pest%theta_pest(k,j,idx1)
        zeta_coords(k,j) = pest%zeta(k,j,idx1)
        theta_indices(k,j) = j+1
        zeta_indices(k,j) = k+pest%n_zeta/2+1
      end do
    end do

    call h5screate_simple_f(rank,dims,space_id,ierr) 
   
    call h5dcreate_f(file_id, "Theta", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, theta_coords, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Zeta", H5T_NATIVE_DOUBLE, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, zeta_coords, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Theta Indices", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, theta_indices, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5dcreate_f(file_id, "Zeta Indices", H5T_NATIVE_INTEGER, space_id, data_id, ierr)
    call h5dwrite_f(data_id, H5T_NATIVE_INTEGER, zeta_indices, dims, ierr)
    call h5dclose_f(data_id, ierr)

    call h5sclose_f(space_id,ierr)

  end subroutine


  subroutine write_mapping_file_surf_data_hdf5(file_id,pest,idx1,data_name,surf_data)
    integer(hid_t), intent(in) :: file_id
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(len=32), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2), intent(in) :: surf_data
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2) :: surf_interp
    integer :: i, j, jp1, k, iunit_tz, theta_index, ierr
    real(dp) :: theta, theta_j, theta_jp1, theta_interp, dt1, dt2, delta
    real(dp), parameter :: pi2 = pi+pi
    real(dp), parameter :: eps = 1e-8

    integer(hid_t) :: data_id, space_id
    integer, parameter :: rank = 2
    integer(hsize_t), dimension(2) :: dims

    dims(1) = pest%n_alpha
    dims(2) = pest%n_zeta-1
    do j=pest%idx_alpha_1,pest%idx_alpha_2
      do k=pest%idx_zeta_1,pest%idx_zeta_2-1
        if (j .lt. pest%idx_alpha_2) then 
          jp1 = j+1 
        else 
          jp1 = pest%idx_alpha_1
        end if
        theta = 0.
        theta_j = pest%alpha(k,j,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        theta_jp1 = pest%alpha(k,jp1,idx1) + pest%iota(idx1)*pest%zeta(k,j,idx1)
        if (theta_j .lt. (0.0 - eps)) then
          theta_j = pi2 + theta_j
        end if
        if (theta_jp1 .lt. (0.0 - eps)) then 
          theta_jp1 = pi2 + theta_jp1
        end if
        i = 0
        do while (theta .lt. (theta_j - eps))
          i = i + 1
          theta = real(i)*pi2/real(pest%n_alpha)
        end do
        theta_j = mod(theta_j,pi2)
        theta_jp1 = mod(theta_jp1,pi2)

        theta_index = mod(i,pest%n_alpha)
        theta_interp = real(theta_index)*pi2/real(pest%n_alpha)
        dt1 = theta_interp - theta_j
        if (dt1 .lt. -eps) then
          dt1 = dt1 + pi2
        end if
        dt2 = theta_jp1 - theta_interp
        if (dt2 .lt. -eps) then
          dt2 = dt2 + pi2
        end if       
        surf_interp(k,j) = real(pest%n_alpha)/pi2*(dt2*surf_data(k,j) + dt1*surf_data(k,jp1))

      end do
    end do
    call h5screate_simple_f(rank,dims,space_id,ierr) 
   
    call h5dcreate_f(file_id, trim(data_name), H5T_NATIVE_DOUBLE, space_id, data_id, ierr)

    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, surf_interp, dims, ierr)

    call h5dclose_f(data_id, ierr)

    call h5sclose_f(space_id,ierr)
  end subroutine

  subroutine write_nonuniform_mapping_file_surf_data_hdf5(file_id,pest,idx1,data_name,surf_data)
    integer(hid_t), intent(in) :: file_id
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(len=32), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2), intent(in) :: surf_data
    integer :: ierr

    integer(hid_t) :: data_id, space_id
    integer, parameter :: rank = 2
    integer(hsize_t), dimension(2) :: dims

    dims(1) = pest%n_alpha
    dims(2) = pest%n_zeta-1

    call h5screate_simple_f(rank,dims,space_id,ierr) 
   
    call h5dcreate_f(file_id, trim(data_name), H5T_NATIVE_DOUBLE, space_id, data_id, ierr)

    call h5dwrite_f(data_id, H5T_NATIVE_DOUBLE, surf_data, dims, ierr)

    call h5dclose_f(data_id, ierr)

    call h5sclose_f(space_id,ierr)
  end subroutine

end module
